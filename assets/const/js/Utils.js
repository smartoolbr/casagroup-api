Array.prototype.lastIndex = function(){
    return this.length > 0 ? this[this.length -1] : [];
};

const Mask = {
    ClearMask:function(value){ return value.match(/\w/g).join(""); },
    OnlyNumber:function(value){ return value.match(/\d+/g).join(""); },
    MaskMobile:function(val){
        if(val.length > 0){
            const clearValue = Mask.ClearMask(val);
            let number = val.length > 10 ? `${val.slice(2,7)}-${val.slice(7,11)}` : `${val.slice(2,6)}-${val.slice(6,10)}`;
            let ddd = `${val.slice(0,2)}`;
            return val.length == 0 ?  "" : `(${ddd}) ${number}`;
        }
        return val;
    },
    Mobile:function(value){
        if(value.length > 1){
            const clearValue = value.match(/\w/g).join("");
            console.log(clearValue);
                 if( clearValue.length > 6 ) return "(" + clearValue.slice(0,2) + ") "+ clearValue.slice(2, clearValue.length == 11 ? 7 : 6) + "-" + clearValue.slice(clearValue.length == 11 ? 7 : 6, clearValue.length == 11 ? 11: 10);
            else if( clearValue.length > 2 ) return "(" + clearValue.slice(0,2) + ") "+ clearValue.slice(2,6);
            else if( clearValue.length > 1 ) return "(" + clearValue.slice(0,2) + ") ";
            // else if( clearValue.length > 1 ) return "(" + clearValue.slice(0,2);
            else if( clearValue.length > 0 ) return "(" + clearValue;
        }
        return value;
    },
    DateMonthYear:function(value){
        if(value){
            const clearValue = Mask.ClearMask(value);
                 if( clearValue.length > 2  ) return clearValue.slice(0,2) + "/" + clearValue.slice(2,6);
            else if( clearValue.length > 0  ) return clearValue;
        }
        return value;
    },
    DateTime:function(value) {
        let result = {
            Br: "",
            Us: "",
            isValid:false
        }
        if(value.length > 0){
            const cv = Mask.ClearMask(value);
            var today = new Date();
            
            let h,m,s,dd,mm,yy;
            
            h = cv.slice(0,2).length == 2 ? cv.slice(0,2) > 23 ? 23 : cv.slice(0,2) : cv.slice(0,2);
            m = cv.slice(2,4).length == 2 ? cv.slice(2,4) > 59 ? 59 : cv.slice(2,4) : cv.slice(2,4);
            s = cv.slice(4,6).length == 2 ? cv.slice(4,6) > 59 ? 59 : cv.slice(4,6) : cv.slice(4,6);
            dd = cv.slice(6,8).length == 2 ? cv.slice(6,8) > 31 ? 31 : cv.slice(6,8) : cv.slice(6,8);
            mm = cv.slice(8,10).length == 2 ? cv.slice(8,10) > 12 ? 12 : cv.slice(8,10) : cv.slice(8,10);
            yy = cv.slice(10,14).length == 4 ? cv.slice(10,14) < today.getFullYear() ? today.getFullYear() : cv.slice(10,14) : cv.slice(10,14);

                if(cv.length > 10)  result.Br = `${h}:${m}:${s} ${dd}/${mm}/${yy}`;
            else if(cv.length > 8)  result.Br = `${h}:${m}:${s} ${dd}/${mm}`;
            else if(cv.length > 6)  result.Br = `${h}:${m}:${s} ${dd}`;
            else if(cv.length > 4)  result.Br = `${h}:${m}:${s}`;
            else if(cv.length > 2)  result.Br = `${h}:${m}`;
            else if(cv.length > 0)  result.Br = `${h}`;

            if(result.Br.length == 19) { result.Us = `${yy}-${mm}-${dd} ${leftPad(h)}:${leftPad(m)}:00`; result.isValid = true; }
        }
        return result;
    },
    Birthday:function(value){
        if(value){
            const clearValue = Mask.ClearMask(value);
                 if( clearValue.length > 4  ) return clearValue.slice(0,2) + "/" + clearValue.slice(2,4) + "/" + clearValue.slice(4,8);
            else if( clearValue.length > 2  ) return clearValue.slice(0,2) + "/" + clearValue.slice(2,4);
            else if( clearValue.length > 0  ) return clearValue;
        }
        return value;
    },
    CPF:function(value){
        if(value){
            const clearValue = Mask.ClearMask(value);
                 if( clearValue.length > 9  ) return clearValue.slice(0,3) + "." + clearValue.slice(3,6) + "." + clearValue.slice(6,9) + "-" + clearValue.slice(9,11);
            else if( clearValue.length > 6  ) return clearValue.slice(0,3) + "." + clearValue.slice(3,6) + "." + clearValue.slice(6,9);
            else if( clearValue.length > 3  ) return clearValue.slice(0,3) + "." + clearValue.slice(3,6);
            else if( clearValue.length > 0  ) return clearValue;
        }
        return value;
    },
    CEP:function(value){
        if(value){
            const clearValue = Mask.ClearMask(value);
                 if( clearValue.length > 4  ) return clearValue.slice(0,5) + "-" + clearValue.slice(5,8);
            else if( clearValue.length > 0  ) return clearValue;
        }
        return value;
    },
    Currency:function(value){
        if(value){
            let clearValue = parseInt(Mask.OnlyNumber(value));
            return (clearValue / 100).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL',})
        }
    },
    DateBR:function(value, sizeYear = 2){
        // 2021-08-03 08:28:12
        if(value) {
            let splited = value.split(" ");
            let year = sizeYear == 2 ? splited[0].split("-")[0].slice(2,4) : splited[0].split("-")[0];
            let date = `${splited[0].split("-")[2]}/${splited[0].split("-")[1]}/${year}`;
            return `${splited[1]} ${date}`;
        }
        return "";
    },
    Percentagem:function(value){
        if(value.length > 0) {
            return `${Mask.ClearMask(value)} %`;
        }
    }
}

const Validation = {
    ClearMask:function(value){ if( value == undefined || value == null || value == "" ) return false; return value.match(/\w/g).join(""); },
    Email:function( email ) {
        let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String( email ).toLowerCase());
    }
}

function leftPad(value){ 
    if(parseInt(value) == 0) {
        return "00";
    }
    return parseInt(value) > 0 && parseInt(value) < 10 ? `0${parseInt(value)}` : parseInt(value); 
}
function getFormattedTime() {
    var today = new Date();
    return today.getDate() + "-" + leftPad(today.getMonth() + 1) + "-" + today.getFullYear() + " " + leftPad(today.getHours()) + "-" + leftPad(today.getMinutes()) + "-" + leftPad(today.getSeconds());
}
