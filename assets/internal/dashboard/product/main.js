const BASE_URI = `${window.location.protocol}//${window.location.host}/${window.location.pathname.split('/')[1]}`;
const MAX_LINES = 6;

$(document).ready(function(){
    // console.log(BASE_URI);

    Machine.Initialize();

});

const MachineItem = {
    itemId:-1,
    position:{ x:-1,y:-1 }
}

const Machine = {
    interno:{
        machineID:-1,
        line:[],
        content:null,
        countline:0
    },
    Initialize:function(){
        const root = this;
        root.AddLine();
    },
    AddLine:function(item){
        const root = this;
        if(root.interno.countline+1 <= 6)
        {
            root.interno.countline++;
            root.DrawLines();
        }
    },
    RemoveLine:function(item){
        const root = this;
        if(root.interno.countline -1 > 0) 
        {
            root.interno.countline--;
            const index = $(item).data('line');
            $(item).remove();
            $('.line_content')[index].remove();

            $('.line_content').each(function(i, e){  $(e).attr('data-line',i); });
            $('.removeline').each(function(i, e){  $(e).attr('data-line',i); });
        }

        if(MAX_LINES-1 == root.interno.countline) 
        {
            $('.machine_content_control').append(`<div class="line_control mt2 addline"></div>`);
            $('.machine_content_control > .addline').on('click', function(){ root.AddLine(); });
        }

    },
    DrawItensLine:function(index){
        const root = this;
        if($('.machine_content').children().length < root.interno.countline)
        {
            $('.machine_content').append(`<div class="line_content" data-line=${root.interno.countline-1}></div>`);
        }
    },
    DrawLines:function(){
        const root = this;
        const addline = `<div class="line_control mt2 addline"></div>`;
        $('.machine_content_control').html("");

        for (let i = 0; i < root.interno.countline; i++) $('.machine_content_control').append(`<div class="line_control mt2 removeline" data-line=${i}></div>`);

        if(MAX_LINES != root.interno.countline) $('.machine_content_control').append(addline);
        $('.machine_content').append(`<div class="line_content" data-line=${root.interno.countline-1}></div>`);

        $('.machine_content_control > .addline').on('click', function(){ root.AddLine(); });
        $('.machine_content_control > .removeline').on('click', function(){ root.RemoveLine(this); });
    }

}

