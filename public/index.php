<?php

    use DI\Container;
    use Slim\Factory\AppFactory;

    require dirname(__FILE__,2) .'/vendor/autoload.php';

    $container = new Container();
    AppFactory::setContainer($container);

    $app = AppFactory::create();
    
    // REMOVE THIS LINE TO UPLOAD TO FTP
    $app->setBasePath('/casagroup');
    
    // all settings
    $settings = require '../App/Helpers/settings.php';
    $settings($container);

    // middleware
    $middleware = require '../App/Middlewares/middleware.php';
    $middleware($app);

    // error
    $error = require '../App/Helpers/error.php';
    $error($app);

    // all routes
    $routes = require '../App/routes/routes.php';
    $routes($app);

    $app->run();
?>