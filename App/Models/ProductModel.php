<?php

    namespace App\Models;

    use App\Dao\DataLayer;

    final class ProductModel extends DataLayer 
    {
        public function __construct()
        {
            parent::__construct("product", ["name", "size", "type", "user_id_created"]);
        }
    }