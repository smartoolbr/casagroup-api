<?php

    namespace App\Models;

    use App\Dao\DataLayer;
    use App\Models\CompanyEmployeeMoldel;

    class ContactModel extends DataLayer
    {
        public function __construct()
        {
            parent::__construct("contact", []);
        }
        
        public function Add(string $country, string $ddd, string $mobile, UserModel $employee) : ContactModel
        {
            $this->country = $country;
            $this->ddd = $ddd;
            $this->mobile = $mobile;
            $this->company_employee_id = $employee->id;
            $this->save();

            return $this;
        }
    }