<?php

    namespace App\Models;

    use App\Dao\DataLayer;

    class UfModel extends DataLayer
    {
        public function __construct()
        {
            parent::__construct("uf", [], "id", false);
        }
    }