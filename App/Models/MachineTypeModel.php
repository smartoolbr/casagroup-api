<?php

    namespace App\Models;

    use App\Dao\DataLayer;

    class MachineTypeModel extends DataLayer
    {
        public function __construct()
        {
            parent::__construct("machine_type", [], "id", false);
        }
    }