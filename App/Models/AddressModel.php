<?php

    namespace App\Models;

    use App\Dao\DataLayer;

    final class AddressModel extends DataLayer
    {
        public function __construct()
        {
            parent::__construct("address", ["name", "address", "number", "district", "state", "city"]);
        }

        public function Add(
            UserModel $employee,
            string $name,
            string $address,
            string $number,
            ?string $address_complement,
            string $district,
            string $state,
            string $city,
            string $zip_code
        ) : AddressModel
        {
            $this->name = $name;
            $this->address = $address;
            $this->number = $number;
            $this->address_complement = $address_complement;
            $this->district = $district;
            $this->state = $state;
            $this->city = $city;
            $this->zip_code = $zip_code;
            $this->company_employee_id = $employee->id;

            $this->save();

            return $this;
        }
    }