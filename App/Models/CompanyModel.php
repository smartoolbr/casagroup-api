<?php

    namespace App\Models;

    use App\Dao\DataLayer;

    class CompanyModel extends DataLayer
    {
        public function __construct()
        {
            parent::__construct("company", []);
        }
        
        public function Add(string $name, string $cnpj, string $email, UserModel $owner, AddressModel $address, ContactModel $contact) : CompanyModel
        {
            $owner->save();

            $address->company_employee_id = $owner->id;
            $address->save();

            $contact->company_employee_id = $owner->id;
            $contact->save();

            $this->name = $name;
            $this->cnpj = $cnpj;
            $this->email = $email;
            $this->owner_id = $owner->id;
            $this->save();

            return $this;
        }
    }