<?php

    namespace App\Models;

    use App\Dao\DataLayer;

    final class MachineItemModel extends DataLayer
    {
        public function __construct()
        {
            parent::__construct("machine_item", ["machine_id","product_id"]);
        }

        public function Add(MachineModel $machine, array $prods) : MachineItemModel
        {
            $object = json_decode(json_encode($prods), FALSE);
            $this->machine_id = $machine->id;
            foreach ($object as $prod) {
                $item = new MachineItemModel();
                $item->machine_id = $machine->id;
                $item->product_id = $prod->product_id;
                $item->price = $prod->price;
                $item->save();
            }

            return $this;
        }

    }