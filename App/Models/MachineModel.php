<?php

    namespace App\Models;

    use App\Dao\DataLayer;
    use App\Models\AddressModel;
    use stdClass;

    final class MachineModel extends DataLayer
    {
        function __construct()
        {
            parent::__construct("machine", ["address_id"], "id", true);
        }
        
        public function Add(AddressModel $addr, string $name, string $machineTypeId)
        {
            $this->address_id = $addr->id;
            $this->name = $name;
            $this->machine_type_id = $machineTypeId;
            $this->save();

            return $this;
        }

        public function getAddress()
        {
            $data = (new AddressModel())->find("id = :uid", "uid={$this->address_id}")->fetch(true);
            return $data ? array_map(function($item)
            { 
                $state_name = (new UfModel())->findById($item->state);
                if($state_name) $item->state_name = $state_name->name;
                return $item->data(); 
            }, $data) : [];
        }

        public function getProducts()
        {
            $data = (new MachineItemModel())->find("machine_id= :umid", "umid={$this->id}")->fetch(true);
            return $data ? array_map(function($item)
            { 
                $nitem = (new ProductModel())->findById($item->product_id);
                $nitem = $nitem->data();
                $nitem->price = $item->price;
                $nitem->position_order = $item->position_order;
                return $nitem; 
            }, $data ) : [];
        }

        public function All()
        {
            $mac = new stdClass();
            $mac->machine = $this->data();
            $mac->address = $this->getAddress();
            $mac->prduct = $this->getProducts();
            return $mac;
        }

    }