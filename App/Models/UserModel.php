<?php

    namespace App\Models;

    use App\Models\ContactModel;
    use App\Dao\DataLayer;
    use App\Support\Email;
    use Slim\Views\Twig;

    class UserModel extends DataLayer
    {

        public function __construct()
        {
            parent::__construct("user", []);
        }

        public function Add(string $name, string $email, string $user, string $password_initial, array $roles, CompanyModel $company) : UserModel
        {
            $this->name = $name;
            $this->email = $email;
            $this->user = $user;
            $this->password_initial = md5($password_initial);
            $this->roles = implode(",", $roles);
            $this->company_id = $company->id;
            $this->save();

            $this->password_initial = $password_initial;

            return $this;
        }

        public function getContact() : array
        {
            $data = (new ContactModel())->find("company_employee_id = :uceid", "uceid={$this->id}")->fetch(true);
            return $data ? array_map(function($item)
            { 
                // $state_name = (new UfModel())->findById($item->state);
                // if($state_name) $item->state_name = $state_name->name;
                return $item->data(); 
            }, $data) : [];
            
        }

        public function sendEmailFirstAccess()
        {
            $data = [ "baseUrl" => getBaseUri(), "user" => "$this->email" , "password" => $this->password_initial, "platform_name" => PLATFORM_NAME ];
            $twig = Twig::create(VIEWS);
            $html = $twig->fetch('emails/newclient.twig', $data);

            $mail = new Email();
            $mail->add("Bem vindo a plataforma ".PLATFORM_NAME, $html,"", $this->email);
            return $mail->toSend(MAIL_NOREPLY_LXP['from_name'],MAIL_NOREPLY_LXP['from_email']);
        }

        public function getAllEmployee(CompanyModel $company, bool $getDisabled = false)
        {
            $data = (new UserModel())->find("company_id=:ucid".$getDisabled ? "" : " AND disabled_at IS NULL", "ucid={$company->id}")->fetch(true);
            return $data ? array_map(function($item)
            { 
                unset($item->password);
                unset($item->password_initial);
                unset($item->pin);
                return $item->data(); 
            }, $data) : [];
        }

        public function exist(string $email) : bool
        {
            return (new UserModel())->find("email= :uem AND disabled_at IS NULL", "uem={$email}")->count() > 0;
        }

        public function login(string $email, string $password) : ?string 
        {
            $data = (new UserModel())->find("email=:uem AND disabled_at IS NULL", "uem={$email}")->fetch(true);
            if(!$data) return null;



            print_r($data);

            return "";
        }
    }