<?php

    namespace App\Support;

    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\SMTP;
    use stdClass;

    final class Email {

        protected $mail;
        private $data;
        private $error;

        public function __construct()
        {
            $this->mail = new PHPMailer(true);
            $this->data = new stdClass();

            $this->mail->isSMTP();
            $this->mail->isHTML();
            $this->mail->setLanguage("br");
            $this->mail->SMTPAuth = true;
            $this->mail->SMTPSecure = "tls";
            $this->mail->CharSet = "utf-8";

            $this->mail->SMTPDebug = SMTP::DEBUG_OFF;
            $this->mail->Mailer = 'smtp';
            
            $this->mail->Host = MAIL_NOREPLY_LXP['host'];
            $this->mail->Port = MAIL_NOREPLY_LXP['port'];
            $this->mail->Username = MAIL_NOREPLY_LXP['user'];
            $this->mail->Password = MAIL_NOREPLY_LXP['passwd'];
            // $this->mail->Priority = 3

        }        

        public function add(string $subject, string $body, string $recipient_name, string $recipient_email)
        {
            $this->data->subject = $subject;
            $this->data->body = $body;
            $this->data->recipient_name = $recipient_name;
            $this->data->recipient_email = $recipient_email;
            return $this;
        }
        
        public function attach(string $filePath, string $fileName)
        {
            $this->data->attach[$filePath] = $fileName; 
            return $this;
        }

        public function toSend(string $from_name, string $from_email) :bool
        {
            try {
                $this->mail->Subject = $this->data->subject;
                $this->mail->msgHTML($this->data->body);
                $this->mail->addAddress($this->data->recipient_email,$this->data->recipient_name);
                $this->mail->setFrom($from_email,$from_name);

                if( !empty($this->data->attach) ){
                    foreach($this->data->attach as $path => $name){
                        $this->mail->addAttachment($path, $name);
                    }
                }
                $this->mail->send();
                // $this->mail->Debugoutput = function($str, $level) {echo "debug level $level; message: $str";};
                return true;
            } catch (\Exception $th) {
                $this->error = $th;
                return false;
            }
        }

        public function error(): ?\Exception
        {
            return $this->error;
        }

    }
