<?php

    use Slim\Psr7\Request;
    use Slim\Psr7\Response;

    return function($app)
    {

        // $site = require '../App/routes/site.php';
        // $site($app);

        // $costumer = require '../App/routes/costumer.php';
        // $costumer($app);

        $services = require '../App/routes/services.php';
        $services($app);

        $internal = require '../App/routes/internal.php';
        $internal($app);

        $app->get('/teste', function(Request $request, Response $response, $args){
            $response = $response->withHeader("Content-Type", "application/json");
            $response->getBody()->write('Home Hello World');
            return $response;
        });

    };
    