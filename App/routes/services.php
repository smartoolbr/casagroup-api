<?php

use App\Controllers\AuthController;
use App\Controllers\CompanyController;
use App\Controllers\MachineController;
use Slim\App;
use Slim\Psr7\Request;
use Slim\Psr7\Response;
use App\Controllers\ProductController;

return function (App $app)
{
    date_default_timezone_set('America/Sao_Paulo');
    session_start();

    $app->get('/test', function(Request $request, Response $response, $args){
        $response = $response->withHeader("Content-Type", "application/json");
        $response->getBody()->write(json_encode(["defaultpath" => getBaseUri()] ));
        
        return $response;
    });

    $app->get('/api/online', function(Request $request, Response $response, $args){
        $response = $response->withHeader("Content-Type", "application/json");
        $response->getBody()->write(json_encode(["success" => true] ));
        return $response;
    });

    $app->post('/api/auth/login',               AuthController::class . ':login');

    $app->post('/api/company/newcompany',       CompanyController::class . ':AddNewCompany');
    $app->post('/api/company/newemployee',      CompanyController::class . ':AddNewEmployee');
    $app->post('/api/company/getemployee',      CompanyController::class . ':GetCompanyEmployee');
    
    $app->post('/api/product',                  ProductController::class . ':addProduct' );
    $app->post('/api/product/getall',           ProductController::class . ':getAll' );
    $app->post('/api/machine/getmachine',       MachineController::class . ':getMachine' );
    $app->post('/api/machine/createmachine',    MachineController::class . ':setMachine' );
    $app->post('/api/machine/getitens',         MachineController::class . ':getMachineItem' );
    $app->post('/api/machine/setmachineitem',   MachineController::class . ':setMachineItem' );

};