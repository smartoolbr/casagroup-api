<?php

namespace App\Controllers;

use Psr\Container\ContainerInterface;
use Slim\Psr7\Request;
use Slim\Psr7\Response;

final class InternalView
{
    private $container;
    // constructor receives container instance
    function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
    
    public function Produtcs(Request $request, Response $response, $args)
    {
        $data = [ "baseUrl" => getBaseUri() ];
        return $this->container->get('view')->render($response, 'internal/dashboard/product/index.twig', $data);
    }

}

    