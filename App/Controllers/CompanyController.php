<?php

    namespace App\Controllers;

    use App\Helpers\CodeGen;
    use App\Models\AddressModel;
    use App\Models\UserModel;
    use App\Models\CompanyModel;
    use App\Models\ContactModel;
    use App\Support\Email;
    use Psr\Container\ContainerInterface;
    use Slim\Psr7\Request;
    use Slim\Psr7\Response;

    final class CompanyController
    {
        private $container;
        
        // constructor receives container instance
        function __construct(ContainerInterface $container)
        {
            $this->container = $container;
        }

        /**
         * Add new company, not allow email duplicated
         */
        public function AddNewCompany(Request $request, Response $response, $args)
        {   
            $response = $response->withHeader("Content-Type", "application/json");
            $rbody = (object)$request->getParsedBody();
            
            $addCompany = (object)$rbody->company;

            if((new CompanyModel())->find("email= :uemail", "uemail={$addCompany['email']}")->fetch(true))
            {
                $response->getBody()->write(json_encode([ "success" => false, "message" => "Desculpe, {$addCompany->email} já esta em uso!" ], JSON_UNESCAPED_UNICODE));
                return $response;
            }

            $newCompany = new CompanyModel();
            $newCompany->name = $addCompany->name;
            $newCompany->cnpj = $addCompany->cnpj;
            $newCompany->email = $addCompany->email;
            $newCompany->save();
            
            $pass = new CodeGen();
            $pass = $pass->Password(8);
            $newEmployee = new UserModel();
            $newEmployee->Add(
                strtolower("user_{$newCompany->name}"),
                $addCompany['email'],
                "user_{$newCompany->email}",
                $pass,
                [4],
                $newCompany
            );

            $newCompany->owner_id = $newEmployee->id;
            $newCompany->save();
            
            $newContact = new ContactModel();
            $addContact = (object)$rbody->contact;
            $newContact->Add(
                $addContact->country,
                $addContact->ddd,
                $addContact->mobile,
                $newEmployee
            );
            
            $addressBody = (object)$rbody->address;
            $newAddress = new AddressModel();
            $newAddress->Add(
                $newEmployee,
                $addressBody->name,
                $addressBody->address,
                $addressBody->number,
                isset($addressBody->complement) ? $addressBody->complement : null,
                $addressBody->district,
                $addressBody->state,
                $addressBody->city,
                $addressBody->zip_code
            );

            $result =      empty($newCompany->fail()) 
                        && empty($newContact->fail()) 
                        && empty($newAddress->fail()) 
                        && empty($newEmployee->fail())
                    ;
            if(!$result)
            {
                $error = [];
                if(!empty($newCompany->fail())) print_r($newCompany->fail()->getMessage());
                if(!empty($newContact->fail())) print_r($newContact->fail()->getMessage());
                if(!empty($newAddress->fail())) print_r($newAddress->fail()->getMessage());
                if(!empty($newEmployee->fail())) print_r($newEmployee->fail()->getMessage());
                
                $response->getBody()->write(json_encode([ "success" => $result, "message" => $error ]));
                return $response;
            }

            $resultEmail = $newEmployee->sendEmailFirstAccess();

            $response->getBody()->write(json_encode([ "success" => $result && $resultEmail ]));
            return $response;
        }

        public function DisableCompany(Request $request, Response $response, $args)
        {
            $response = $response->withHeader("Content-Type", "application/json");
            $rbody = $request->getParsedBody();
            
            $company = (new CompanyModel())->findById($rbody['id']);
            $company->disable();

            $result = empty($company->fail());
            $response->getBody()->write(json_encode([
                "success" => $result
            ]));

            return $response;
        }

        public function AddNewEmployee(Request $request, Response $response, $args)
        {
            $response = $response->withHeader("Content-Type", "application/json");
            $rbody = (object)$request->getParsedBody();

            $addEmployee = (object)$rbody->employee;
            
            if(!$company = (new CompanyModel())->findById($rbody->comp))
            {
                $response->getBody()->write(json_encode([ "success" => false, "message" => "Parametros errados!" ]));
                return $response;
            }

            if((new UserModel())->exist($addEmployee->email))
            {
                $response->getBody()->write(json_encode([ "success" => false, "message" => "Desculpe, {$addEmployee->email} já esta em uso!" ], JSON_UNESCAPED_UNICODE));
                return $response;
            }
            
            $pass = new CodeGen();
            $pass = $pass->Password(8);
            $newEmployee = new UserModel();
            $newEmployee->Add(
                $addEmployee->name,
                $addEmployee->email,
                $addEmployee->email,
                $pass,
                [5],
                $company
            );
            
            $newContact = new ContactModel();
            $addContact = (object)$rbody->contact;
            $newContact->Add(
                $addContact->country,
                $addContact->ddd,
                $addContact->mobile,
                $newEmployee
            );

            $result =   empty($newContact->fail()) && empty($newEmployee->fail());
            if(!$result)
            {
                $error = [];
                if(!empty($newContact->fail())) $error += $newContact->fail()->getMessage();
                if(!empty($newEmployee->fail())) $error += $newEmployee->fail()->getMessage();
                
                $response->getBody()->write(json_encode([ "success" => $result, "message" => $error ]));
                return $response;
            }

            $resultEmail = $newEmployee->sendEmailFirstAccess();

            $response->getBody()->write(json_encode([ "success" => $result && $resultEmail ]));
            return $response;
        }

        /**
         * Disable employee | once time disabled employee, owner must have create another
         *
         * @param Request $request
         * @param Response $response
         * @param [type] $args
         * @return void
         */
        public function DisableEmployee(Request $request, Response $response, $args)
        {
            $response = $response->withHeader("Content-Type", "application/json");
            $rbody = (object)$request->getParsedBody();

            $employee = (new UserModel())->findById($rbody->id);
            $employee->disable();

            if($employee->fail())
            {
                $response->getBody()->write(json_encode([ "success" => false, "message" => $employee->fail()->getMessage() ]));
                return $response;
            }

            $response->getBody()->write(json_encode([ "success" => true ]));
            return $response;
        }

        public function GetCompanyEmployee(Request $request, Response $response, $args)
        {
            $response = $response->withHeader("Content-Type", "application/json");
            $rbody = (object)$request->getParsedBody();

            // print_r($rbody->id);

            if(isset($rbody->id))
            {
                if($rbody->id != "all")
                {
                    $comp = (new CompanyModel())->findById($rbody->id);
                    $employee = (new UserModel())->getAllEmployee($comp);
                    print_r($employee);
                }
            }

            return $response;
        }
    }