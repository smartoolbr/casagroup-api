<?php

    namespace App\Controllers;

    use App\Models\ProductModel;
    use Slim\Psr7\Request;
    use Slim\Psr7\Response;

    final class ProductController
    {
        private ProductModel $model;

        function __construct()
        {
            $this->model = new ProductModel();
        }

        public function AddProduct(Request $request, Response $response, $args)
        {
            $response = $response->withHeader("Content-Type", "application/json");
            $reqbody = $request->getParsedBody();
            $product = new ProductModel();

            if(isset($reqbody['id']) && !empty($reqbody['id']))
            {
                $product = $product->findById($reqbody['id']);
                if(isset($reqbody['name'])) $this->model->name = ucwords(strtolower($reqbody['name']));
                if(isset($reqbody['size'])) $this->model->size = $reqbody['size'];
                if(isset($reqbody['type'])) $this->model->type = ucwords(strtolower($reqbody['type']));
                $result = $this->model->save();

                $response->getBody()->write(json_encode([
                    "success" => $result,
                    "message" => $result ? "Produto atualizado" : "Desculpe, houve um erro A"
                ], JSON_UNESCAPED_UNICODE));
                return $response;
            }

            $this->model->name = ucwords(strtolower($reqbody['name']));
            $this->model->size = $reqbody['size'];
            $this->model->type = ucwords(strtolower($reqbody['type']));
            $product = $product->find("name = :uname AND size = :usize AND type = :utype", "uname={$this->model->name}&usize={$this->model->size}&utype={$this->model->type}")->fetch(true);

            if($product)
            {
                $response->getBody()->write(json_encode([
                    "success" => true,
                    "message" => "Produto já existe na base de dados"
                ], JSON_UNESCAPED_UNICODE));
                return $response;               
            }
            
            $this->model->user_id_created = $reqbody['userid'];
            $result = $this->model->save();

            if($this->model->fail())
            {
                var_dump($this->model->fail());
            }

            $response->getBody()->write(json_encode([
                "success" => $result,
                "message" => $result ? "Produto criado" : "Desculpe, houve um erro B"
            ], JSON_UNESCAPED_UNICODE));

            return $response;
        }

        public function getAll(Request $request, Response $response, $args)
        {
            $response = $response->withHeader("Content-Type", "application/json");
            
            $prod = (new ProductModel())->find()->fetch(true);

            $response->getBody()->write(json_encode([
                "success" => true,
                "payload" => $prod ? array_map(function($item){ return $item->data(); }, $prod) : []
            ], JSON_UNESCAPED_UNICODE));

            return $response;
        }

    }