<?php

    namespace App\Controllers;

    use Psr\Container\ContainerInterface;
use Slim\Psr7\Request;
use Slim\Psr7\Response;

final class ViewsController
    {
        // (Request $request, Response $response, $args)
        private $container;
        
        // constructor receives container instance
        function __construct(ContainerInterface $container)
        {
            $this->container = $container;
        }

        public function login(Request $request, Response $response, $args)
        {
            $data = ["baseUrl" => getBaseUri()];
            return $this->container->get('view')->render($response, 'costumers/visualizer/index.twig', $data);
        }

        public function registerNewPassword(Request $request, Response $response, $args)
        {
            $data = ["baseUrl" => getBaseUri()];
            return $this->container->get('view')->render($response, 'costumers/visualizer/index.twig', $data);
        }
    }