<?php

    namespace App\Controllers;

    use App\Helpers\JWTManager;
    use App\Models\UserModel;
    use Psr\Container\ContainerInterface;
    use Slim\Psr7\Request;
    use Slim\Psr7\Response;
    use Slim\Routing\RouteContext;

    final class AuthController
    {
        private $container;
        
        // constructor receives container instance
        function __construct(ContainerInterface $container)
        {
            $this->container = $container;
        }

        public function logout(Request $request, Response $response, $args)
        {
            unset($_SESSION['token']);
            unset($_SESSION['name']);
        
            $response->getBody()->write(json_encode([ "success" => true ], JSON_UNESCAPED_UNICODE));
            return $response;
        }
        
        public function login(Request $request, Response $response, $args)
        {
            $response = $response->withHeader("Content-Type", "application/json");
            $rbody = (object)$request->getParsedBody();

            $data = (new UserModel())->find("email=:uem AND disabled_at IS NULL", "uem={$rbody->email}")->fetch(true);
            $routeContext = RouteContext::fromRequest($request);
            $basePath = $routeContext->getBasePath();
            
            // print_r(session_status());
            print_r($basePath);
            return $response;

            if($data)
            {
                $data = $data[0]->data();
                if(empty($data->password))
                {
                    // redirect to write new password by user
                    // $token = JWTManager::createToken($data->id, $data->email, $data->name, $data->roles);
                    // $token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxIiwibmFtZSI6InVzZXJfc21hcnRvb2wiLCJlbWFpbCI6ImZhYmVjYW9AaG90bWFpbC5jb20iLCJyb2xlcyI6IjQiLCJpYXQiOjE2MzE3OTcyOTMsImV4cCI6MTYzMTg0NzU5OX0.ob1CEbeFSTqo08ZYgODJ6fARrxRizZYTBPUnNJPZM1E";
                    // $_SESSION['token'] = $token;
                    // $_SESSION['name'] = $data->name;
                    // $response->getBody()->write(json_encode([ "success" => true ]));



                }
                else 
                {
                    $user_pass = md5($rbody->password);
                    if($user_pass == $data->password)
                    {
                        // $token = JWTManager::createToken($data->id, $data->email, $data->name, $data->roles);
                        // print_r($token);
                    }
                }
            }
            // $email = $rbo
            // print_r($data);
            // print_r(md5($rbody->password));



            return $response;
        }

    }