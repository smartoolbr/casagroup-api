<?php

    namespace App\Controllers;

    use App\Models\AddressModel;
    use App\Models\MachineItemModel;
    use App\Models\MachineModel;
    use App\Models\MachineTypeModel;
    use Slim\Psr7\Request;
    use Slim\Psr7\Response;
    use stdClass;

    final class MachineController
    {
        public function getMachine(Request $request, Response $response, $args)
        {
            $response = $response->withHeader("Content-Type", "application/json");
            $rbody = $request->getParsedBody();
            $result = [];
            $success = false;

            if(isset($rbody['id']))
            {
                if($rbody['id'] == "all")
                {
                    $res = (new MachineModel())->find()->fetch(true);
                    for ($i=0; $i < count($res); $i++) {
                        /** @var MachineModel $res */
                        $us = new stdClass();
                        $us = $res[$i]->data();
                        foreach ($res[$i]->getAddress() as $addr) {
                            $us->address = $addr;
                        }
                        $result[] = $us;
                    }

                    $success = true;
                }
                else 
                {
                    /** @var MachineModel $machine */
                    $machine = (new MachineModel())->findById($rbody['id']);
                    if($machine)
                    {
                        $machineType = (new MachineTypeModel())->findById($machine->machine_type_id);
                        $machine->machine_type_name = $machineType->name;
                        $result = $machine->data();
        
                        foreach ($machine->getAddress() as $m) { $result->address = $m; }
                        $success = true;
                    }
                }
            }
        
            $response->getBody()->write(json_encode([
                "success" => $success,
                "payload" => $result
            ], JSON_UNESCAPED_UNICODE));
            return $response;
        }

        public function SetMachine(Request $request, Response $response, $args)
        {
            $response = $response->withHeader("Content-Type", "application/json");
            $reqbody = $request->getParsedBody();

            $addressBody = $reqbody['address'];
            
            $addressModel = new AddressModel();
            if(isset($addressBody['id'])) { 
                $message[] = "endereço atualizado";
                $addressModel->id = $addressBody['id']; 
            }
            else {
                $message[] = "endereço criado";
            }

            if(isset($addressBody['name'])) $addressModel->name = $addressBody['name'];
            if(isset($addressBody['address'])) $addressModel->address = $addressBody['address'];
            if(isset($addressBody['number'])) $addressModel->number = $addressBody['number'];
            if(isset($addressBody['complement'])) $addressModel->complement = isset($addressBody['complement']) ? $addressBody['complement'] : null;
            if(isset($addressBody['district'])) $addressModel->district = $addressBody['district'];
            if(isset($addressBody['state'])) $addressModel->state = $addressBody['state'];
            if(isset($addressBody['city'])) $addressModel->city = $addressBody['city'];
            if(isset($addressBody['zip_code'])) $addressModel->zip_code = $addressBody['zip_code'];
            $addressModel->save();

            if($addressModel->fail()) {
                $response->getBody()->write(json_encode([
                    "success" => false,
                    "message" => $addressModel->fail()
                ], JSON_UNESCAPED_UNICODE));
                return $response;
            }

            $machineBody = $reqbody['machine'];
            $machineModel = new MachineModel();
            
            if(isset($machineBody['id']))
            {
                $ma = $machineModel->findById($machineBody['id']);
                $ma->name = $machineBody['name'];
                $ma->machine_type_id = $machineBody['machine_type_id'];
                $ma->address_id = $addressModel->data()->id;
                $ma->active = $machineBody['active'];
                $ma->save();
                
                if($ma->fail()) {
                    $response->getBody()->write(json_encode([
                        "success" => false,
                        "message" => $ma->fail()->getMessage()
                    ], JSON_UNESCAPED_UNICODE));
                    return $response;
                }

                $message[] = "machine atualizada";
            }
            else 
            {
                $machineModel->name = $machineBody['name'];
                $machineModel->machine_type_id = $machineBody['machine_type_id'];
                $machineModel->address_id = $addressModel->data()->id;
                $machineModel->active = $machineBody['active'];
                $machineModel->save();
                
                if($machineModel->fail()) {
                    $response->getBody()->write(json_encode([
                        "success" => false,
                        "message" => $machineModel->fail()->getMessage()
                    ], JSON_UNESCAPED_UNICODE));
                    return $response;
                }

                $message[] = "machine criado";
            }

            $response->getBody()->write(json_encode([
                "success" => true,
                "message" => implode(" e ", $message)
            ], JSON_UNESCAPED_UNICODE));

            return $response;
        }

        public function setMachineItem(Request $request, Response $response, $args)
        {
            $response = $response->withHeader("Content-Type", "application/json");
            $reqbody = $request->getParsedBody();
            
            if( isset($reqbody['machine_id']) && isset($reqbody['prods']) )
            {
                $item = new MachineItemModel();
                $machine = (new MachineModel())->findById($reqbody['machine_id']);
                $item->Add($machine, $reqbody['prods']);
                
                if($item->fail())
                {
                    $response->getBody()->write(json_encode([
                        "success" => false,
                        "message" => $item->fail()->getMessage()
                    ], JSON_UNESCAPED_UNICODE));
                    return $response;
                }

                $response->getBody()->write(json_encode([
                    "success" => true,
                    "message" => "Produto(s) adicionado(s) com sucesso"
                ], JSON_UNESCAPED_UNICODE));
                return $response;
            }

            $response->getBody()->write(json_encode([
                "success" => false,
                "message" => "Error on params"
            ], JSON_UNESCAPED_UNICODE));
            return $response;
        }

        public function getMachineItem(Request $request, Response $response, $args)
        {
            $response = $response->withHeader("Content-Type", "application/json");
            $reqbody = $request->getParsedBody();
            $success = false;
            $products = [];

            if(isset($reqbody['id'])){
                /** @var MachineModel $machine */
                $machine = (new MachineModel())->findById($reqbody['id']);
                if($machine)
                {
                    $products = $machine->getProducts();
                    $success = true;
                }
            }

            $response->getBody()->write(json_encode([
                "success" => $success,
                "payload" => $products
            ], JSON_UNESCAPED_UNICODE));
            return $response;
        }
    }

