<?php

use App\Middleware\CorsMiddleware;
use Slim\App;
use Slim\Views\TwigMiddleware;

return function(App $app)
{
    $app->addRoutingMiddleware();
    $app->addBodyParsingMiddleware();
    $app->add(TwigMiddleware::createFromContainer($app));
};