<?php

namespace App\Helpers;

final class JWTManager
{
    // public static function CreateToken($email,$name,$id,$roles,$secretary){
    public static function createToken($id,$email,$name,$roles) : ?string
    {
        date_default_timezone_set('Brazil/East');
        
        // Create token header as a JSON string
        $header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);

        // Create token payload as a JSON string
        $payload = json_encode([
            "sub" => $id,
            // "scy" => $secretary,
            "name" => $name,
            "email" => $email,
            "roles" => $roles,
            "iat" => time(),
            // "permissions" => "",
            "exp" => strtotime(date("Y-m-d H:i:s",mktime (23, 59, 59, date("m"), date("d"), date("Y")))),
        ]);

        // Encode Header to Base64Url String
        $base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));

        // Encode Payload to Base64Url String
        $base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));

        // Create Signature Hash
        $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, JWT_SECRET, true);

        // Encode Signature to Base64Url String
        $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));

        // Create JWT
        return $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;
        // return $payload;
    }

    public static function checkToken(string $token)
    {
        date_default_timezone_set('Brazil/East');

        if( is_null($token) ) return false;

        $tokens = explode(".",$token);

        if( count($tokens) != 3 ) return false;
        
        $Header = $tokens[0];
        $Payload = $tokens[1];
        $payload_token = self::getPayload($token);
        
        // Create Signature Hash and signature by token
        $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode( hash_hmac('sha256', $Header . "." . $Payload, JWT_SECRET, true) ));
        $Signature = $tokens[2];

        return $Signature == $base64UrlSignature && time() < $payload_token->exp;
    }

    public static function getPayload($token) : ?object
    {
        if(!$token) return null;
        return json_decode(utf8_decode(base64_decode(explode(".",$token)[1])));
    }
}


