<?php

    use DI\Container;
    use Psr\Container\ContainerInterface;
    use Slim\Views\Twig;

    define("PLATFORM_NAME", "PLATFORM X");

    define("MAIL_NOREPLY",[
        "host" => "mail.smartool.com.br",
        "port" => "587",
        "user" => "naoresponda@smartool.com.br",
        "passwd" => "bBLwr&)lKj3u(7<at*32",
        "from_name" => "Smartool",
        "from_email" => "naoresponda@smartool.com.br",
    ]);

    define("MAIL_NOREPLY_LXP",[
        "host" => "br324.hostgator.com.br",
        "port" => "26",
        "user" => "noreply@lpxp.com.br",
        "passwd" => "zaDY0}+P8l2]",
        "from_name" => "LivePhotoXp",
        "from_email" => "noreply@lpxp.com.br",
    ]);

    define("DATA_LAYER_CONFIG", [
        "driver" => "mysql",
        "host" => "localhost",
        // "host" => "108.167.132.39:3306",
        "port" => "3306",
        "dbname" => "smart741_cgroup",
        "username" => "smart741_cg_mg",
        "passwd" => '4d@aaLtwQ{wu',
        "options" => [
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
            PDO::ATTR_CASE => PDO::CASE_NATURAL
        ]
    ]);

    define('ROOT', dirname(__FILE__,3));
    define("ROOT_FILES", dirname(__FILE__,3).'/Storage/');
    define("ROOT_DOWNLOAD", dirname(__FILE__,3).'/Downloads/');
    define('VIEWS', ROOT . '/App/Views/');
    define('JWT_SECRET', hash_hmac('sha256', date("Y-m-d").'l1v4', true));

    return function(Container $container)
    {
        $container->set('settings', function(){
            return [
                'displayErrorDetails' => true,
                'logErrorDetails' => true,
                'logErrors' => true,
            ];
        });
        
        $container->set('view', function(){
            return Twig::create(VIEWS);
        });
    };

    function getBaseUri()
    {
        $escaped_url = "";

        if ($_SERVER['HTTP_HOST'] == 'localhost')
        {
            $url = explode('/', $_SERVER['REQUEST_URI'])[1];
            $url =  "{$_SERVER["REQUEST_SCHEME"]}://{$_SERVER['HTTP_HOST']}/{$url}";
            $escaped_url = htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' );
            return $escaped_url;
        }
        else
        {
            $url =  "{$_SERVER["REQUEST_SCHEME"]}://{$_SERVER['HTTP_HOST']}/livexp";
            $escaped_url = htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' );
            return $escaped_url;
        }
    }

    function getDirContents($dir, &$results = array()) {
        $files = scandir($dir);
    
        foreach ($files as $key => $value) {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if (!is_dir($path)) {
                $results[] = $path;
            } else if ($value != "." && $value != "..") {
                getDirContents($path, $results);
                $results[] = $path;
            }
        }
    
        return $results;
    }

    function getPathFileInFolder($baseFolder, $fileName) : ?string
    {
        if(empty($fileName)) return null;

        $paths = getDirContents($baseFolder);

        for ($i=0; $i < count($paths); $i++) { 
            $item = explode('\\', $paths[$i])[count(explode('\\', $paths[$i])) - 1];
            if(strpos($item, $fileName) !== false){
                return $paths[$i];
            }
        }
    }

    function leftPad($value)
    {
        return intval($value) < 10 ? "0".$value : $value;
    }

    function getNewVersionFile()
    {
        // $date = getdate()['year'] . leftPad(getdate()['mon']) . leftPad(getdate()['mday']);
        // return "0.{$date}";
        return "0.0";
    }