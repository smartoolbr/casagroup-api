<?php

    namespace App\Helpers;

    final class CodeGen
    {
        private function Generator(bool $isVoucher){
            $customAlphabet = !$isVoucher ? '02468ACEGILNPRTVY' : '13579BDFHJLNOQRTUXZ' ;
            return $customAlphabet;
        }

        public function Password(int $sizeVoucher)
        {
            $alpha = "0123456789ABCDEFGHIJKLMNPQRSTUVXZ|_*@#$";
            $generator = new RandomStringGenerator($alpha);
            $generator->setAlphabet($alpha);
            return $generator->generate( $sizeVoucher );
        }

        public function Voucher(int $countVoucher, int $sizeVoucher, string $sizeconsume = NULL) : array
        {
            // $alpha = CodeGenerator::Generator(true);
            $alpha = $this->Generator(true);

            // Set initial alphabet.
            $generator = new RandomStringGenerator($alpha);

            // Change alphabet whenever needed.
            $generator->setAlphabet($alpha);
            $vouchers = [];
            for ($i=0; $i < $countVoucher; $i++) { 
                array_push($vouchers, $generator->generate( $sizeVoucher, $sizeconsume ) ); 
            }

            return $vouchers;
        }

        public function Code(int $countVoucher, int $sizeVoucher) : array
        {
            $alpha = $this->Generator(true);

            // Set initial alphabet.
            $generator = new RandomStringGenerator($alpha);

            // Change alphabet whenever needed.
            $generator->setAlphabet($alpha);
            $vouchers = [];
            for ($i=0; $i < $countVoucher; $i++) { 
                array_push($vouchers, $generator->generate( $sizeVoucher ) ); 
            }

            return $vouchers;
        }
    }