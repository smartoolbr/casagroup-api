<?php

    namespace App\Helpers;

    final class StorageSupport
    {
        private static function getDefaultPathStorage(){
            return ROOT_FILES;
        }
    
        private static function CheckDefaultPathStorage(){
            return is_dir(self::getDefaultPathStorage());
        }
    
        private static function CreateDefaultPathStorage(){
            // echo is_dir(self::getDefaultPathStorage()) ? "true" : "false";
            return mkdir(self::getDefaultPathStorage());
        }

        public static function GenerateHashUser($name){
            // return md5($name . $crm . $uf . time());
            return md5($name . time());
        }
    
        public static function CheckUserPath($hashUser){
            return is_dir(self::getDefaultPathStorage() . $hashUser);
        }
    
        public static function CreateUserPath($hashUser)
        {
            if(!self::CheckDefaultPathStorage())
            {
                if(!self::CreateDefaultPathStorage())
                    return false;
            }

            if(!self::CheckUserPath($hashUser))
            {
                return mkdir(self::getDefaultPathStorage() . $hashUser, 0777, true);
            }

            return false;
        }

        public static function RemoveFile($fileName, $default_path){
            $file = self::getDefaultPathStorage() . "$default_path/$fileName";
            if ( file_exists( $file ) ) {
                return unlink( $file );
            } else {
                return false;
            }
        }

        public static function DestroyDataUser($hashUser)
        {
            $dir = self::getDefaultPathStorage() . $hashUser;
            if (is_dir($dir)) {
                $objects = scandir($dir);
                foreach ($objects as $object) {
                    if ($object != "." && $object != "..") {
                        echo $object.'<br>';
                        if (filetype($dir."/".$object) == "dir") rmdir($dir."/".$object); else unlink($dir."/".$object);
                    }
                }
                reset($objects);
                return rmdir($dir);
            } 
            return true;
        }

        public static function Save($fileName, $default_path, $autoname = true){
            if( is_uploaded_file( $_FILES[ $fileName ]['tmp_name'] )){
                $source_path = $_FILES[ $fileName ]['tmp_name'];
                $extention = strtolower( pathinfo($_FILES[ $fileName ]['name'],PATHINFO_EXTENSION) );
                $new_name_file = $autoname ? md5(time() . $_FILES[ $fileName ]['name']) . '.' . $extention : $_FILES[ $fileName ]['name'];

                $directory = self::getDefaultPathStorage() . $default_path . "/";

                if( !self::CheckUserPath ($default_path) ) self::CreateUserPath ($default_path);

                if( move_uploaded_file($source_path, $directory . $new_name_file) ){
                    return $directory . $new_name_file;
                }else{
                    return null;
                }
            }
            else{
                return null;
            }
        }

        public static function GetNameFileFromPath($path){
            $temp = explode('/', $path);
            return $temp[count($temp)-1];
        }
    }