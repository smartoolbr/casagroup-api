<?php

use Slim\App;
use Slim\Psr7\Response;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpMethodNotAllowedException;
use Slim\Psr7\Request;

return function(App $app)
{
    $setting = $app->getContainer()->get('settings');
    $errorMiddleware = $app->addErrorMiddleware( $setting['displayErrorDetails'],$setting['logErrorDetails'],$setting['logErrors'] );
    $errorMiddleware->setErrorHandler(
        HttpNotFoundException::class,
        function (Request $request, Throwable $exception, bool $displayErrorDetails) use ($app){
            $response = new Response();
            $data = ["baseUrl" => getBaseUri()];
            return $app->getContainer()->get('view')->render($response, 'error/default_error.twig', $data)->withStatus(404);
        });

    $errorMiddleware->setErrorHandler(
        HttpMethodNotAllowedException::class,
        function (Request $request, Throwable $exception, bool $displayErrorDetails) {
            $response = new Response();
            $response->getBody()->write('405 NOT ALLOWED');
            return $response->withStatus(405);
        });
};